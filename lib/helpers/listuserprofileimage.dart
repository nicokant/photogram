import 'package:flutter/material.dart';
import 'dart:typed_data';

class ListUserProfileImage extends StatelessWidget {
  ListUserProfileImage({
    this.image,
  });

  final Uint8List image;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: image != null ? Image.memory(
        image,
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ) : Image(
        image: AssetImage('assets/anon.png'),
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ),
    );
  }
}
