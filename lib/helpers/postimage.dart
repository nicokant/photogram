import 'package:flutter/material.dart';
import 'dart:typed_data';

class PostImage extends StatelessWidget {
  PostImage({
    this.image,
  });

  final Uint8List image;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.memory(
          image,
          fit: BoxFit.cover,
      ),
    );
  }
}
