import 'dart:typed_data';
import 'dart:convert';

Uint8List convertToB64(dynamic val) {
  try {
    return Base64Codec().decode(val);
  } catch (error) {
    return null;
  }
}

class Post {
  final String msg;
  final Uint8List img;
  final String user;
  final DateTime timestamp;
  final bool imgValid;

  Post({
    this.msg,
    this.img,
    this.user,
    this.timestamp,
    this.imgValid,
  });

  formatDate() {
    return timestamp.day.toString() + '/' + timestamp.month.toString() + '/' + timestamp.year.toString();
  }

  factory Post.fromJson(Map<String, dynamic> json) {
    Uint8List img = convertToB64(json['img']);
    return Post(
      msg: json['msg'] as String,
      img: img,
      user: json['user'] as String,
      timestamp: DateTime.parse(json['timestamp']),
      imgValid: img != null,
    );
  }
}