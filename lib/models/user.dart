import 'dart:typed_data';
import 'dart:convert';

Uint8List convertToB64(dynamic val) {
  try {
    return Base64Codec().decode(val);
  } catch (error) {
    return null;
  }
}

class User {
  final String name;
  final Uint8List picture;
  bool followed;

  User({
    this.name,
    this.picture,
    this.followed,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['name'] as String,
      picture: convertToB64(json['picture']),
      followed: false,
    );
  }
}