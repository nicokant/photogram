import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'models/user.dart';
import 'helpers/listuserprofileimage.dart';

class SearchPage extends StatefulWidget {
  SearchPage({
    Key key,
    this.session_id,
  });

  String session_id;

  @override
  State<StatefulWidget> createState() {
    return SearchState();
  }
}

class SearchState extends State<SearchPage> {
  TextEditingController _searchInput = TextEditingController();
  List<User> _userlist = new List();
  List<String> _followed = new List();

  @override
  void initState() {
    syncFollowed();
    super.initState();
  }

  void syncFollowed () async {
    var body = { "session_id": widget.session_id };
    var response = await http.post('https://ewserver.di.unimi.it/mobicomp/fotogram/followed', body: body);
    final followed = jsonDecode(response.body)['followed'];

    _followed = followed.map<String>((json) => User.fromJson(json).name).toList();
  }

  void _getNames(String text) async {
    var body = {
      "usernamestart": text,
      "session_id": widget.session_id,
    };
    print(body);

    var response = await http.post('https://ewserver.di.unimi.it/mobicomp/fotogram/users', body: body);

    final users = jsonDecode(response.body)['users'];

    setState(() {
      _userlist = users.map<User>((json) {
        User u = User.fromJson(json);
        if (_followed.contains(u.name)) {
          u.followed = true;
        }
        return u;
      }).toList();
    });
  }

  void toggleFollowing(User u) async {
    String url = u.followed ? 'https://ewserver.di.unimi.it/mobicomp/fotogram/unfollow' : 'https://ewserver.di.unimi.it/mobicomp/fotogram/follow';
    var body = { "session_id": widget.session_id, "username": u.name };
    var response = await http.post(url, body: body);

    if (response.statusCode > 200) {
      return;
    }

    setState((){
      _userlist = _userlist.map<User>((User user) {
        if (user.name == u.name) {
          return User(name: user.name, followed: !user.followed, picture: user.picture);
        }
        return user;
      }).toList();
    });

    syncFollowed();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: new Container(child: Icon(Icons.search, size: 30.0,)),
        title: TextField(
          autofocus: true,
          controller: _searchInput,
          decoration: InputDecoration(
            hintText: 'Search...',
            border: InputBorder.none,
          ),
          onChanged: _getNames,
          style: new TextStyle(
            fontSize: 22.0,
            color: Colors.black87,
          ),
        ),
        actions: [
          new IconButton(
            icon: Icon(Icons.close),
            iconSize: 30.0,
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: ListView.builder(
          itemCount: _userlist.length,
          itemBuilder: (BuildContext context, int index) {
            User current = _userlist[index];
            return new ListTile(
              contentPadding: EdgeInsets.all(10.0),
              title: Text(current.name),
              leading: ListUserProfileImage(image: current.picture),
              trailing: IconButton(
                  icon: Icon(current.followed ? Icons.favorite : Icons.favorite_border),
                  color: Colors.black87,
                  onPressed: () { toggleFollowing(current); },
              ),
            );
          }
      ),
    );
  }
}