import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dashboard.dart';


class LoginPage extends StatefulWidget {
  LoginPage();

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginPage> {
  bool _isLoading = false;
  TextEditingController _userInput = TextEditingController();
  TextEditingController _passInput = TextEditingController();
  BuildContext _scaffoldContext;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();


  void _fetchLogin(BuildContext context) async {
    String user = _userInput.text;
    String pass = _passInput.text;

    var response = await http.post('https://ewserver.di.unimi.it/mobicomp/fotogram/login', body: { "username": user, "password": pass });

    if (response.statusCode == 200) {
      final SharedPreferences prefs = await _prefs;
      prefs.setString('session_id', response.body);

      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return DashboardPage(session_id: response.body);
      }));


    } else {
      final snackBar = SnackBar(content: Text('Wrong credentials'));
      Scaffold.of(_scaffoldContext).showSnackBar(snackBar);
      _passInput.clear();
    }
  }

  @override
  void dispose() {
    _userInput.dispose();
    _passInput.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 50.0),
          child: Column(
            children: <Widget>[
              Center(child:
              Text('Photogram',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              ),
              Container(
                child: TextFormField(
                  controller: _userInput,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    hintText: 'Your username',
                    labelText: 'Username',
                  ),
                ),
                margin: EdgeInsets.only(top: 10),
              ),
              Container(
                child: TextFormField(
                  controller: _passInput,
                  obscureText: true,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.lock),
                    hintText: 'Your secret passcode',
                    labelText: 'Password',
                  ),
                ),
                margin: EdgeInsets.only(bottom: 30),
              ),
              RaisedButton(
                child: Center(child: Text('Login'),),
                onPressed: () { _fetchLogin(context); },
              )
            ],
          ),
        ),
      ],
    );

    return Scaffold(
        body: new Builder(
          builder: (BuildContext context) {
            _scaffoldContext = context;
            return body;
          },
        )
    );
  }

}
