import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dashboard.dart';


checkAuthentication () async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('session_id') ?? '';
}

class SplashPage extends StatelessWidget {
  SplashPage();

  @override
  Widget build(BuildContext context) {

    checkAuthentication().then((token) {
      if (token != '') {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
          return DashboardPage(session_id: token);
        }));
      } else {
        Navigator.pushReplacementNamed(context, '/login');
      }
    });

    return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(child: CircularProgressIndicator())
          ],
        )
    );
  }
}
