import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'search.dart';
import 'tabs/wall.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage({
    Key key,
    this.session_id,
  });

  final String session_id;

  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends State<DashboardPage> {
  int _index = 0;
  List<Widget> tabs = List<Widget>();

  @override
  void initState() {
    super.initState();
    setState((){
      tabs = [
        Center(child: Wall(session_id: widget.session_id,)),
        Center(child: Text('Add')),
        Center(child: Text('Profile')),
      ];
    });
  }

  void logout (BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('session_id');
    Navigator.pushReplacementNamed(context, '/login');
  }

  void openSearch (BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SearchPage(session_id: widget.session_id);
    }));
  }

  void _goToTab(int index) {
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Icon(Icons.camera, size: 30.0,)
                ),
                Text('Photogram')
              ],
          ),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              iconSize: 30.0,
              onPressed: () { openSearch(context); },
            )
          ],
        ),
        body: tabs[_index],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _index,
          iconSize: 30.0,
          type: BottomNavigationBarType.fixed,
          fixedColor: Colors.black,
          onTap: _goToTab,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_a_photo),
              title: Text(''),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person),
                title: Text('')
            ),
          ],
        ),
    );
  }


}

