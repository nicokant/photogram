import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../models/post.dart';
import '../helpers/postimage.dart';

class Wall extends StatefulWidget {
  Wall({
    this.session_id,
  });

  final String session_id;

  @override
  State<StatefulWidget> createState() {
    return WallState();
  }
}

class WallState extends State<Wall> {
  List _posts = List();
  bool isLoading = true;

  void _loadPosts() async {
    var body = {
      "session_id": widget.session_id,
    };

    setState((){
      isLoading = true;
    });
    var response = await http.post('https://ewserver.di.unimi.it/mobicomp/fotogram/wall', body: body);
    final posts = jsonDecode(response.body)['posts'];

    print(posts);

    setState((){
      _posts = posts.map((json) => Post.fromJson(json)).toList();
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadPosts();
  }

  Widget buildPost(BuildContext ctx, int i) {
    Post p = _posts[i];
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
      child: Card(
        elevation: 0.0,
      shape: Border.all(color: Colors.black87),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          p.imgValid ? PostImage(image: p.img) : Container(),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(p.user, style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                )),
                Text(p.formatDate()),
              ],
            ),
            subtitle: Text(p.msg, style: TextStyle(
              fontSize: 18.0,
            )),
          ),
        ],
      ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(child: CircularProgressIndicator())
        ],
      );
    }
    return ListView.builder(
        itemCount: _posts.length,
        itemBuilder: buildPost,
    );
  }
}